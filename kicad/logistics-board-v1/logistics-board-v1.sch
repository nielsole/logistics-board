EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x04_Female J2
U 1 1 608E920A
P 1800 4400
F 0 "J2" H 1828 4376 50  0000 L CNN
F 1 "JST Front Left" H 1828 4285 50  0000 L CNN
F 2 "Connector_JST:JST_NV_B04P-NV_1x04_P5.00mm_Vertical" H 1800 4400 50  0001 C CNN
F 3 "~" H 1800 4400 50  0001 C CNN
	1    1800 4400
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J3
U 1 1 608E96A4
P 3950 4400
F 0 "J3" H 3978 4376 50  0000 L CNN
F 1 "JST Front Right" H 3978 4285 50  0000 L CNN
F 2 "Connector_JST:JST_NV_B04P-NV_1x04_P5.00mm_Vertical" H 3950 4400 50  0001 C CNN
F 3 "~" H 3950 4400 50  0001 C CNN
	1    3950 4400
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J1
U 1 1 608E9AF1
P 1750 5300
F 0 "J1" H 1778 5276 50  0000 L CNN
F 1 "JST Back Left" H 1778 5185 50  0000 L CNN
F 2 "Connector_JST:JST_NV_B04P-NV_1x04_P5.00mm_Vertical" H 1750 5300 50  0001 C CNN
F 3 "~" H 1750 5300 50  0001 C CNN
	1    1750 5300
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J4
U 1 1 608E9E77
P 3950 5300
F 0 "J4" H 3978 5276 50  0000 L CNN
F 1 "JST Back Right" H 3978 5185 50  0000 L CNN
F 2 "Connector_JST:JST_NV_B04P-NV_1x04_P5.00mm_Vertical" H 3950 5300 50  0001 C CNN
F 3 "~" H 3950 5300 50  0001 C CNN
	1    3950 5300
	1    0    0    -1  
$EndComp
$Comp
L MCU_Module:WeMos_D1_mini U1
U 1 1 608E695A
P 2900 1950
F 0 "U1" H 2900 1061 50  0000 C CNN
F 1 "WeMos_D1_mini" H 2900 970 50  0000 C CNN
F 2 "Module:WEMOS_D1_mini_light" H 2900 800 50  0001 C CNN
F 3 "https://wiki.wemos.cc/products:d1:d1_mini#documentation" H 1050 800 50  0001 C CNN
	1    2900 1950
	1    0    0    -1  
$EndComp
NoConn ~ 2500 1850
Text GLabel 1600 4300 0    50   Input ~ 0
GND
Text GLabel 1550 5200 0    50   Input ~ 0
GND
Text GLabel 3750 4300 0    50   Input ~ 0
GND
Text GLabel 3750 5200 0    50   Input ~ 0
GND
Text GLabel 1600 4600 0    50   Input ~ 0
V33
Text GLabel 1550 5500 0    50   Input ~ 0
V33
Text GLabel 3750 4600 0    50   Input ~ 0
V33
Text GLabel 3750 5500 0    50   Input ~ 0
V33
Text GLabel 3000 1150 1    50   Input ~ 0
V33
Text GLabel 2800 1150 1    50   Input ~ 0
V5
$Comp
L Connector:Conn_01x03_Female J5
U 1 1 608F6DE8
P 5850 4400
F 0 "J5" H 5878 4426 50  0000 L CNN
F 1 "JST LED Strip" H 5878 4335 50  0000 L CNN
F 2 "Connector_JST:JST_NV_B03P-NV_1x03_P5.00mm_Vertical" H 5850 4400 50  0001 C CNN
F 3 "~" H 5850 4400 50  0001 C CNN
	1    5850 4400
	1    0    0    -1  
$EndComp
Text GLabel 5650 4500 0    50   Input ~ 0
V5
Text GLabel 5650 4300 0    50   Input ~ 0
GND
Text GLabel 5650 4400 0    50   Input ~ 0
LED_D
Text GLabel 2500 1950 0    50   Input ~ 0
LED_D
Text GLabel 2900 2750 3    50   Input ~ 0
GND
Text GLabel 1600 4400 0    50   Input ~ 0
FL_D
Text GLabel 1600 4500 0    50   Input ~ 0
FL_C
Text GLabel 1550 5300 0    50   Input ~ 0
BL_D
Text GLabel 1550 5400 0    50   Input ~ 0
BL_C
Text GLabel 3750 4400 0    50   Input ~ 0
FR_D
Text GLabel 3750 4500 0    50   Input ~ 0
FR_C
Text GLabel 3750 5300 0    50   Input ~ 0
BR_D
Text GLabel 3750 5400 0    50   Input ~ 0
BR_C
$Comp
L Device:CP_Small 100uF1
U 1 1 608FE5EC
P 4800 1400
F 0 "100uF1" H 4888 1446 50  0000 L CNN
F 1 "CP_Small" H 4888 1355 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_Tantal_D5.5mm_P2.50mm" H 4800 1400 50  0001 C CNN
F 3 "~" H 4800 1400 50  0001 C CNN
	1    4800 1400
	1    0    0    -1  
$EndComp
Text GLabel 4800 1300 1    50   Input ~ 0
V33
Text GLabel 4800 1500 3    50   Input ~ 0
GND
$Comp
L Sensor:DHT11 U2
U 1 1 60901FF5
P 4850 2500
F 0 "U2" H 4606 2546 50  0000 R CNN
F 1 "DHT11" H 4606 2455 50  0000 R CNN
F 2 "Sensor:Aosong_DHT11_5.5x12.0_P2.54mm" H 4850 2100 50  0001 C CNN
F 3 "http://akizukidenshi.com/download/ds/aosong/DHT11.pdf" H 5000 2750 50  0001 C CNN
	1    4850 2500
	1    0    0    -1  
$EndComp
Text GLabel 4850 2200 1    50   Input ~ 0
V33
Text GLabel 5150 2500 2    50   Input ~ 0
DHT22_D
Text GLabel 4850 2800 3    50   Input ~ 0
GND
Text GLabel 3300 1750 2    50   Input ~ 0
FL_C
Text GLabel 3300 1850 2    50   Input ~ 0
BL_D
Text GLabel 3300 1950 2    50   Input ~ 0
BL_C
Text GLabel 3300 2050 2    50   Input ~ 0
FR_D
Text GLabel 3300 2250 2    50   Input ~ 0
BR_D
Text GLabel 3300 2150 2    50   Input ~ 0
BR_C
NoConn ~ 3300 1450
Text GLabel 3300 2350 2    50   Input ~ 0
DHT22_D
NoConn ~ -3800 -1900
Text GLabel 3300 1550 2    50   Input ~ 0
FR_C
Text GLabel 3300 1650 2    50   Input ~ 0
FL_D
$Comp
L Mechanical:MountingHole H2
U 1 1 6092B379
P 6550 2350
F 0 "H2" H 6650 2396 50  0000 L CNN
F 1 "MountingHole" H 6650 2305 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 6550 2350 50  0001 C CNN
F 3 "~" H 6550 2350 50  0001 C CNN
	1    6550 2350
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 6092B547
P 6550 2150
F 0 "H1" H 6650 2196 50  0000 L CNN
F 1 "MountingHole" H 6650 2105 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 6550 2150 50  0001 C CNN
F 3 "~" H 6550 2150 50  0001 C CNN
	1    6550 2150
	1    0    0    -1  
$EndComp
$EndSCHEMATC
