How will all the different board communicate with eachother?

Requirements:
- Many participants on same line (Bus)
- must work without twisted pair
- 10 meter line length
- very low throughput
- low-cost/simple implementation, ideally bit-bangable / native support
- ground offsets tolerated (not truly required if one master per PSU)

From these requirements the following is favoured:
- differential pair

CAN
- cannot be bitbanged
- ground offsets tolerated https://electronics.stackexchange.com/questions/106265/maximum-i2c-bus-length

I2C
- for intra-PCB communication, cables possible
- widely supported
- address assignment must not have collisions
- SMBus is subset of I2C

- not differential: Don't use twisted pair https://electronics.stackexchange.com/questions/106265/maximum-i2c-bus-length#comment712758_106303

I2s

RS232
- not a bus

RS485
- is a bus
- cannot be bitbanged
- ground offsets can be tolerated https://electronics.stackexchange.com/questions/106265/maximum-i2c-bus-length

SPI

UART
- line length theoretically possible: https://electronics.stackexchange.com/questions/106265/maximum-i2c-bus-length


General remarks