Each level in the cupboard has its own µC that aggregates sensor data and controls the LED strip.
All cupboards share an I2C bus.
A raspberrypi is the master with the source of truth data.
This ensures the following:
* All communication is wired -> high reliability, no dependence on WiFi
* database is local -> not dependence on Internet availability
* 