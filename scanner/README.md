mosquitto-clients needs to be installed

logkeys needs to be compiled from source


Script to generate mosquitto events:

```
logkeys --no-daemon -s -d $(readlink /dev/input/by-id/usb-13ba_Barcode_Reader-event-kbd | cut -d '/' -f 2) -o - --no-func-keys --no-timestamps | 
    while read -r line
    do
        mosquitto_pub -h 78.46.190.60 -u admin -P $PASSWORD -t iot/storage/storage/scanner -m $line
    done
```