Compile needs to be run every time before uploading, otherwise you get outdated binaries :/

## Arduino nano

arduino-cli core install arduino:avr
arduino-cli compile -b arduino:avr:nano Blink
arduino-cli upload -p /dev/ttyUSB0 -b arduino:avr:nano:cpu=atmega328old -t -v Blink


## Wemos D1 Mini

* Make sure config was initialized.
* Add to ~/.arduino15:

```
board_manager:
  additional_urls: ["http://arduino.esp8266.com/stable/package_esp8266com_index.json"]
```

```
arduino-cli core install esp8266:esp8266
arduino-cli compile -b esp8266:esp8266:d1_mini Blink
arduino-cli upload -p /dev/ttyUSB0 -b esp8266:esp8266:d1_mini -t -v Blink
```

```
arduino-cli lib install FastLED
arduino-cli lib install PubSubClient
``` 