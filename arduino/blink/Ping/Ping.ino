#include <SoftwareSerial.h>

#define D3 6
#define D4 7

SoftwareSerial mySerial =  SoftwareSerial(D3, D4);


void setup() {
    pinMode(D3, INPUT);
    pinMode(D4, OUTPUT);
    mySerial.begin(9600);
    Serial.begin(9600);
    Serial.print("Initialized");
}

void loop() {
    mySerial.print("Ping");
    mySerial.println();
    mySerial.listen();
    char c = mySerial.read();
    if (c != -1) {
        Serial.print("doh");
        Serial.println();
    }
    
    

}
