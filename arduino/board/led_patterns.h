

void updateLEDs_sinus(){
  unsigned long time = millis();
  int16_t sinoid = sin16_C(time * 10);
  int fractional_led_space = 32767 / NUM_LEDS;
  for(int i = 0; i < NUM_LEDS; i++) {
    if(lightstate_on) {
      if (i * fractional_led_space < sinoid) {
        leds[i] = primaryColor;
      }else {
        leds[i] = backgroundColor;
      }
    }else{
      leds[i] = CRGB::Black;
    }
  }
}

void updateLEDs_blink(){
  unsigned long time = millis();
  CRGB color = ((time / 300) % 2) ? primaryColor : backgroundColor;
  for(int i = 0; i < NUM_LEDS; i++) {
    if(lightstate_on) {
      leds[i] = color;
    }else{
      leds[i] = CRGB::Black;
    }
  }
}

void updateLEDs_bars(){
  unsigned long time = millis();
  float threshold = (time % 1008) / 42;
  for(int i = 0; i < NUM_LEDS; i++) {
    if ((i % 24) < threshold) {
      leds[i] = primaryColor;
    }else {
      leds[i] = backgroundColor;
    }
    uint8_t fraction = 255 / (threshold - ((int) threshold) / 24);
    blend(leds[((int) threshold) + 1], primaryColor, fraction);
  }
}

void updateLEDs() {
  // Having everything in their own function avoids cross-initialization issues
  switch (lightMode)
  {
  case LIGHTMODE_SINUS:
    updateLEDs_sinus();
    break;
  case LIGHTMODE_BLINK:
    updateLEDs_blink();
    break;
  case LIGHTMODE_BARS:
    updateLEDs_bars();
    break;
  default:
    break;
  }
  FastLED.show();
}