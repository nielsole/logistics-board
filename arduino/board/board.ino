
#include "HX711.h"
#include "secrets.h"
#include "hx711util.h"

#include "FastLED.h"
#include "FastLED_RGBW.h"
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <PubSubClient.h>

# define NUM_CELLS 4
# define NUM_READINGS 5
# define MAX_WEIGHT_TOTAL 40000
HX711 loadcell[NUM_CELLS];
float readings[NUM_CELLS][NUM_READINGS];
uint loop_iteration = 0;
long last_sensor_read = 0;
float last_stable_read_total = 0;
float last_stable_read_weights[NUM_CELLS];
float candidate_weight = 0;
long last_candidate_weight_change = 0;
long last_significant_change = 0;

float delta_weights[NUM_CELLS];
float d_x;
float d_y;

bool lightstate_on;

enum LightMode {
    LIGHTMODE_NORMAL
};
LightMode lightMode = LIGHTMODE_NORMAL;

float change_event_x_start;
float change_event_x_end;
long change_event_received;

WiFiClient espWiFiClient;
PubSubClient pubSubClient(espWiFiClient);
#define MSG_BUFFER_SIZE	(50)
char msg[MSG_BUFFER_SIZE];
const char* pubSubPrefix = "iot/storage/storage/";
const char* pubSubWildcard = "iot/storage/storage/#";
const char* pubSubChanges = "iot/storage/storage/changes";
const char* pubSubReset = "iot/storage/storage/reset";
String clientId;

#define DATA_PIN 15                                             // Serial data pin
#define COLOR_ORDER GRB                                       // It's GRB for WS2812B and GBR for APA102
#define LED_TYPE WS2812                                       // What kind of strip are you using (APA102, WS2801 or WS2812B)?
#define NUM_LEDS 46 //24                                           // Number of LED's


 
// FastLED with RGBW
CRGBW leds[NUM_LEDS];
CRGB *ledsRGB = (CRGB *) &leds[0];


void rainbow_wave(uint8_t thisSpeed, uint8_t deltaHue) {     // The fill_rainbow call doesn't support brightness levels.
 
// uint8_t thisHue = beatsin8(thisSpeed,0,255);                // A simple rainbow wave.
 uint8_t thisHue = beat8(thisSpeed,255);                     // A simple rainbow march.
  
 fill_rainbow(ledsRGB, getRGBWsize(NUM_LEDS), thisHue, deltaHue);            // Use FastLED's fill_rainbow routine.
 
} // rainbow_wave()

int map_to_led(float ordinate) {
    int led_offset = ordinate * NUM_LEDS;
    led_offset = min(led_offset, NUM_LEDS - 1);
    led_offset = max(led_offset, 0);
    return led_offset;
}

void render() {
  long current_time = millis();
  if (current_time < 12000){
    rainbow_wave(10, 10);                                      // Speed, delta hue values.
  }else {
    int on_led = map_to_led(d_x);
    // pulled out of tight loop for performance reasons
    int x_start_offset = map_to_led(change_event_x_start);
    int x_end_offset = map_to_led(change_event_x_end);
    for (int i = 0; i < NUM_LEDS; i++) {
      if(on_led == i && current_time - last_significant_change < 10000) {
        if (((uint16) ((current_time - last_candidate_weight_change) / 100)) % 2) {
          // More than 100ms have passed and we are blinking
          if(sum(delta_weights, 4) > 0) {
            // Blue for adding weight (green is taken by success action)
            leds[i] = CRGBW(0, 0, 255, 0);
          }else {
            // Red for removing weight
            leds[i] = CRGBW(255, 0, 0, 0);
          }
        }else {
          // we are blinking
          leds[i] = CRGBW(0, 0, 0, 255);
        }
      }else {
        if(lightstate_on) {
          leds[i] = CRGBW(0, 0, 0, 15);
        }else {
          leds[i] = CRGBW(0, 0, 0, 0);
        }
        if(current_time - change_event_received < 300) {
          if(x_start_offset <= i && x_end_offset >= i) {
            leds[i] = CRGBW(0, 255, 0, 0);
          }
        }
        
      }
    }
  }
  FastLED.show();
}


void reconnect() {
  // Loop until we're reconnected
  while (!pubSubClient.connected()) {
    // Attempt to connect
    if (pubSubClient.connect(clientId.c_str(), pubSubUser, pubSubPass)) {
      // Once connected, publish an announcement...
      pubSubClient.publish(pubSubReset, "true");
      // ... and resubscribe
      pubSubClient.subscribe(pubSubWildcard);
    } else {
      // Wait 5 seconds before retrying
      // TODO Ain't nobody got time for that
      delay(5000);
    }
  }
}

void callback(char* topic, byte* payload, unsigned int length) {
    // TODO Receiving messages comes later
    // TODO Refactor into a library
  
  // Switch on the LED if an 1 was received as first character
  char* subTopic = topic + strlen(pubSubPrefix);
  // TODO: Check if length is 1 smaller to buffer, so that 0 byte still can be safely set.
  char* pointer = ((char(*))payload) + length;
  *pointer = 0;  
  
  if(strcmp(subTopic, "light/enabled") == 0)
  {
    // Casting from unsigned int to int should be save, as it is bounded by 4.
    lightstate_on = (strncmp((char(*)) payload, "true", min((int) length,4)) == 0);
  } else if(strcmp(subTopic, "light/brightness") == 0)
  {
    // TODO use generic integer parsing now that null byte is in place
    uint8 globalBrightness = strtoul((char(*)) payload, 0, 10);
    FastLED.setBrightness(globalBrightness);
  } else if(strcmp(subTopic, "light/change") == 0)
  {
    // X_START,X_END,Y_START,Y_END,WEIGHT,DELTAAMOUNT
    // TODO use generic integer parsing now that null byte is in place
    char* x_start_s = strtok((char(*)) payload, ",");
    char* x_end_s = strtok(NULL, ",");
    change_event_x_start = strtof(x_start_s, NULL);
    change_event_x_end = strtof(x_end_s, NULL);
    change_event_received = millis();
  }else if(strcmp(subTopic, "light/mode") == 0)
  {
    String mode = String((char(*))payload);
    if(mode == "normal"){
      lightMode = LIGHTMODE_NORMAL;
    }
  }
}

void setup() {
    Serial.begin(9600);
    pinMode(A0, INPUT) ;
    Serial.println("Hello World. Connecting to WiFi...");

    // Create a unique client ID
    clientId = "ESP8266-Storage-" + String(ESP.getChipId(), HEX);
    WiFi.softAPdisconnect(true);
    WiFi.begin(wifiSSID, wifiPass);
    while (WiFi.status() != WL_CONNECTED) { // 3 == connected
      Serial.println("Could not connect to WiFi. Retrying..." + String(WiFi.status()));
      // WiFi.begin does not actually seem to do any work
      delay(1000);
    }
    Serial.println("Connected to WiFi");
    Serial.println("Configuring MQTT...");
    pubSubClient.setServer(pubSubHost, 1883);
    pubSubClient.setCallback(callback);
    Serial.println("Initializing loadcells...");
    // D7, D0 Front Right
    loadcell[0].begin(13, 16);
    // D6, D5 Back Right
    loadcell[1].begin(12, 14);
    // D4, D3 Front Left
    loadcell[2].begin(2, 0);
    // D2, D1 Back Left
    loadcell[3].begin(4, 5);
    delay(0);
    // not a global because it is stored in the structs of the loadcells
    float factors[NUM_CELLS] = {111.7f,109.8f,111.5f,114.5f}; //initial board: 249.662f, 239.688f, 248.367f, 243.850f};
    for(int i = 0; i < NUM_CELLS; i++) {
        Serial.println("Initializing loadcell " + String(i) + "...");
        loadcell[i].set_scale(factors[i]);
        loadcell[i].tare(5);
    }
    Serial.println("Device name: " + clientId);
    //FastLED with RGBW
    FastLED.addLeds<LED_TYPE, DATA_PIN, RGB>(ledsRGB, getRGBWsize(NUM_LEDS));
    Serial.println("Setup completed");
}

float sum(float samples[], int length) {
    float ret = 0.f;
    for (int i = 0; i < length; i++) {
        ret += samples[i];
    }
    return ret;
}

float average(float samples[], int length) {
    return sum(samples, length) / length;
}

float _center(float lower, float upper) {
    if (lower == 0.f || upper == 0.f) {
        return 0.5f;
    }
    return upper / (lower + upper);
}

float center_x(float samples[]) {
    float left = samples[0] + samples[1];
    float right = samples[2] + samples[3];
    return _center(left, right);
}

float center_y(float samples[]) {
    float front = samples[0] + samples[3];
    float back = samples[1] + samples[2];
    return _center(front, back);
}

void loop() {
    if (!pubSubClient.connected()) {
        reconnect();
    }
    // TODO run only every x milliseconds
    pubSubClient.loop();
    long current_time = millis();
    // Deal with wraparound
    if (current_time < last_sensor_read) {
        last_sensor_read = 0;
    }
    if (current_time - last_sensor_read > 100) {
        Serial.print("Weights:");
        float current_weights[NUM_CELLS];
        for(int i = 0; i < NUM_CELLS; i++) {
            
            if (loadcell[i].is_ready()) {
                float grams = loadcell[i].get_units(1);
                readings[i][loop_iteration % NUM_READINGS] = grams;
                current_weights[i] = average(readings[i], NUM_READINGS);
                Serial.print("\t(" + String(i) + ") ");
                Serial.print(current_weights[i], 2);
            } else {
                Serial.print("\t" + String(i) + ") ERROR");
            }
        }

        float total_weight = sum(current_weights, NUM_CELLS);
        // Initialization
        if(last_stable_read_total == 0.f) {
            last_stable_read_total = total_weight;
        }
        if(candidate_weight == 0.f || last_candidate_weight_change > current_time) {
            candidate_weight = total_weight;
            last_candidate_weight_change = current_time;
        }
        // Throw away and set candidate weight on rapid changes
        if(abs(total_weight - candidate_weight) > 3) {
            candidate_weight = total_weight;
            last_candidate_weight_change = current_time;
        }
        // Invalid state
        if(total_weight > MAX_WEIGHT_TOTAL) {
            candidate_weight = 0;
            last_candidate_weight_change = current_time;
        }
        // Gracefully handle reordering of items (breaks removal of these items, but makes adding/removing other items more reliable)
        if(abs(total_weight - last_stable_read_total) < 3) {
            /* TODO Determine where an item was removed and where it is placed now
               This might be hard to do, when the item is not lifted up inbetween
            */
            memcpy(last_stable_read_weights, current_weights, sizeof(current_weights));
        }
        // Prevent frequent updates when there would be no change required
        if (abs(last_stable_read_total - candidate_weight) < 20) {
            last_candidate_weight_change = current_time;
        }else{
          last_significant_change = current_time;
        }
        float c_x = center_x(current_weights);
        float c_y = center_y(current_weights);
        for(int i = 0; i < NUM_CELLS; i++) {
          delta_weights[i] = current_weights[i] - last_stable_read_weights[i];
        }
        d_x = center_x(delta_weights);
        d_y = center_y(delta_weights);
        Serial.print("\tX: ");
        Serial.print(c_x, 2);
        Serial.print("\tY: ");
        Serial.print(c_y, 2);
        Serial.print("\tTotal: ");
        Serial.print(total_weight, 2);
        Serial.print("\tCandidate: ");
        Serial.print(candidate_weight, 2);
        Serial.print("\r\n");

        if (current_time - last_candidate_weight_change > 700) {

            last_candidate_weight_change = current_time;
            last_stable_read_total = total_weight;
            // Equivalent: last_stable_read_weights = current_weights;
            memcpy(last_stable_read_weights, current_weights, sizeof(current_weights));
            Serial.print("Change detected. New weight: ");
            Serial.print(last_stable_read_total, 2);
            Serial.print("\tX: ");
            Serial.print(d_x, 2);
            Serial.print("\tY: ");
            Serial.print(d_y, 2);
            Serial.print("\r\n");
            String change_string = clientId + "," + String(sum(delta_weights, NUM_CELLS)) + "," + String(d_x) + "," + String(d_y);
            pubSubClient.publish(pubSubChanges, change_string.c_str());
        }
        loop_iteration++;
        last_sensor_read = current_time;
    }
    render();
    // 1s / 80 = 12.5ms
    delay(14);
  
}

