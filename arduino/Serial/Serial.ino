
// Back Left: 4,5
// Front Left: 2,0
// Back Right: 12,14
// Front Right: 13,16
#include "HX711.h"

// HX711 circuit wiring
const int LOADCELL_DOUT_PIN = 13;
const int LOADCELL_SCK_PIN = 16;

HX711 scale;

void setup() {
    Serial.begin(9600);
    scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
    delay(0);
    //scale.set_scale(111.5f);
    //scale.tare(5);
}

void loop() {

  if (scale.is_ready()) {
    long reading = scale.read();
    // float reading = scale.get_units(1);
    Serial.print("HX711 reading: ");
    Serial.println(reading);
  } else {
    Serial.println("HX711 not found.");
  }

  delay(1000);
  
}