package main

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestPrometheus(t *testing.T) {
	psqlconn := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", "db", "5432", "postgres", "postgres", "postgres")
	client, _ := sql.Open("postgres", psqlconn)
	err := client.Ping()
	if err != nil {
		t.Fatal("Could not open database connection", err)
	}
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		prometheus_scrape(w, r, client)
	}))
	defer server.Close()
	res, err := http.Get(server.URL)
	if err != nil {
		t.Error(err)
	}
	body_bytes, _ := ioutil.ReadAll(res.Body)
	body_string := string(body_bytes)
	if len(body_string) == 0 {
		t.Error("Expected nonempty response")
	}

}
