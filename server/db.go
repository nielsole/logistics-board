package main

import (
	"database/sql"
	"fmt"
	"log"
	"math"
)

func selectProduct(client *sql.DB, client_id string, weight float64, x float64, y float64) (best_pk int64, best_amount int64, best_x_start float64, best_x_end float64, best_y_start float64, best_y_end float64) {
	// SELECT id, label, weight FROM inventory WHERE x_start < ? AND x_end > ? AND y_start < ? AND y_end > ?
	rows, err := client.Query("SELECT id, label, weight, x_start, x_end, y_start, y_end FROM logistics_slots as slots INNER JOIN logistics_products as products ON slots.product_id = products.id WHERE board_name = $1 AND x_start <= $2 AND x_end >= $3 AND y_start <= $4 AND y_end >= $5", client_id, x, x, y, y)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	var (
		label       string
		row_weight  float64
		row_amount  int64
		pk          int64
		row_x_start float64
		row_x_end   float64
		row_y_start float64
		row_y_end   float64
		best_score  float64
	)
	// TODO Evaluate multiple options (Prefer single items or choose closest match)
	for rows.Next() {
		err := rows.Scan(&pk, &label, &row_weight, &row_x_start, &row_x_end, &row_y_start, &row_y_end)
		if err != nil {
			log.Fatal(err)
		}
		row_amount = int64(math.Round(weight / row_weight))
		deviation := math.Abs(float64(row_amount)*row_weight - weight)
		err_amount := int64(6)
		if row_amount > 0 {
			err_amount = row_amount
		}
		error_score := math.Pow(1.5, deviation/20) + float64(err_amount)
		if best_score == 0.0 || (best_score != 0.0 && error_score < best_score) {
			best_score = error_score
			best_x_start = row_x_start
			best_x_end = row_x_end
			best_y_start = row_y_start
			best_y_end = row_y_end
			best_amount = row_amount
			best_pk = pk
		}
		println("Detected a ", label, ". It has id: ", pk, " Quantity change: ", row_amount, " Score: ", error_score)
	}
	fmt.Printf("%d: %d had best score: %f", best_pk, best_amount, best_score)
	println("")
	return best_pk, best_amount, best_x_start, best_x_end, best_y_start, best_y_end
}
