package main

import (
	"testing"
)

func TestMQTTParsing(t *testing.T) {
	_, _, _, _, err := parse_message("test")
	if err == nil {
		t.Error("Message 'test' should not have passed")
	}
	for _, entry := range []string{"a,0,0,0"} {
		if _, _, _, _, err = parse_message(entry); err != nil {
			t.Errorf("Should have accepted message %s", entry)
		}
	}
}
