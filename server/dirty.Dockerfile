FROM logistics_board
WORKDIR /build
ADD . .
ENV CGO_ENABLED=0
RUN go build -o server .
CMD ["./server"]
