module gitlab.com/nielsole/logistics-board/server

go 1.15

require (
	github.com/eclipse/paho.mqtt.golang v1.3.2
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.0
)
