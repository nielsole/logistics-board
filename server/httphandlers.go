package main

import (
	"database/sql"
	"encoding/csv"
	"fmt"
	"log"
	"net/http"
	"strconv"
)

func dump_data(w http.ResponseWriter, r *http.Request, client *sql.DB) {
	// TODO do this via structs or another nice way, to make it easier to dump to Prometheus
	rows, err := client.Query("SELECT label, sum(amount) FROM  logistics_slots as slots INNER JOIN logistics_products as products ON slots.product_id = products.id GROUP BY label;")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	var (
		label  string
		amount int
	)
	csv_writer := csv.NewWriter(w)
	defer csv_writer.Flush()
	for rows.Next() {
		err := rows.Scan(&label, &amount)
		if err != nil {
			log.Fatal(err)
		}
		csv_writer.Write([]string{label, strconv.Itoa(amount)})
	}
}

func prometheus_scrape(w http.ResponseWriter, r *http.Request, client *sql.DB) {
	// TODO do this via structs or another nice way, to make it easier to dump to Prometheus
	rows, err := client.Query("SELECT board_name, products.weight, amount, label FROM logistics_slots as slots INNER JOIN logistics_products as products ON slots.product_id = products.id;")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	var (
		board  string
		weight float32
		amount int
		label  string
	)
	for rows.Next() {
		err := rows.Scan(&board, &weight, &amount, &label)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Fprintf(w, "inventory_stock_amount{board=\"%s\",label=\"%s\"} %d\n", board, label, amount)
		fmt.Fprintf(w, "inventory_stock_weight{board=\"%s\",label=\"%s\"} %f\n", board, label, weight)
	}
}
