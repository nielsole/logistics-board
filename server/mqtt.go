package main

import (
	"database/sql"
	"errors"
	"fmt"
	"regexp"
	"strconv"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

var connectHandler mqtt.OnConnectHandler = func(client mqtt.Client) {
	fmt.Println("Connected")
}

var connectLostHandler mqtt.ConnectionLostHandler = func(client mqtt.Client, err error) {
	fmt.Printf("Connect lost: %v", err)
}

func parse_message(message string) (string, float64, float64, float64, error) {
	r := regexp.MustCompile(`([\w]+),(\d+\.?\d*),(\d+\.?\d*),(\d+\.?\d*)`)
	split_payload := r.FindStringSubmatch(message)
	// split_payload := strings.Split(payload, ",")
	if len(split_payload) != 5 {
		return "", 0, 0, 0, errors.New("Message did not match expected format")
	}
	client_id := split_payload[0]
	weight, _ := strconv.ParseFloat(split_payload[1], 64)
	x, _ := strconv.ParseFloat(split_payload[2], 64)
	y, _ := strconv.ParseFloat(split_payload[3], 64)
	return client_id, weight, x, y, nil
}

func create_mqtt_change_message_handler(client *sql.DB) mqtt.MessageHandler {
	return func(mqtt_client mqtt.Client, msg mqtt.Message) {
		fmt.Printf("Received message: %s from topic: %s\n", msg.Payload(), msg.Topic())
		payload := string(msg.Payload())
		client_id, weight, x, y, err := parse_message(payload)
		if err != nil {
			fmt.Print(err.Error(), "\n")
		}
		updateDatabase(client, client_id, mqtt_client, weight, x, y)
	}
}

func mqtt_start(mq_host string, mq_port int, mq_user, mq_pass string, client *sql.DB) {
	opts := mqtt.NewClientOptions()
	opts.AddBroker(fmt.Sprintf("tcp://%s:%d", mq_host, mq_port))
	opts.SetClientID("")
	opts.SetUsername(mq_user)
	opts.SetPassword(mq_pass)
	// opts.SetDefaultPublishHandler(message_handler)
	opts.OnConnect = connectHandler
	opts.OnConnectionLost = connectLostHandler
	mqtt_client := mqtt.NewClient(opts)
	if token := mqtt_client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	topic_change := "iot/storage/storage/changes"
	change_message_handler := create_mqtt_change_message_handler(client)
	token := mqtt_client.Subscribe(topic_change, 1, change_message_handler)
	token.Wait()
	fmt.Printf("Subscribed to topic %s\n", topic_change)
	// topic_scanner := "iot/storage/storage/scanner"
	// token = mqtt_client.Subscribe(topic_scanner, 1, scan_message_handler)
	// token.Wait()
}
