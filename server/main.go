package main

import (
	"database/sql"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

func CheckError(err error) {
	if err != nil {
		panic(err)
	}
}

func updateDatabase(client *sql.DB, client_id string, mqtt_client mqtt.Client, weight float64, x float64, y float64) {
	pk, amount, x_start, x_end, _, _ := selectProduct(client, client_id, weight, x, y)
	res, err := client.Exec("UPDATE logistics_slots SET amount = amount + $2 WHERE id = $1", pk, amount)
	if err != nil {
		log.Fatal(err)
	}
	rows_affected, _ := res.RowsAffected()
	println("Updated ", rows_affected, " rows")
	println(client_id, weight, x, y)
	var message = fmt.Sprintf("%f,%f", x_start, x_end)
	mqtt_client.Publish("iot/storage/storage/light/change", 0, false, message)
}

func main() {

	db_name := os.Getenv("DB_NAME")
	db_user := os.Getenv("DB_USER")
	db_pass := os.Getenv("DB_PASS")
	db_host := os.Getenv("DB_HOST")
	db_port := os.Getenv("DB_PORT")
	mq_host := os.Getenv("MQ_HOST")
	mq_user := os.Getenv("MQ_USER")
	mq_pass := os.Getenv("MQ_PASS")

	listen_port := flag.String("port", ":8080", "Listening port")
	mqtt_port := flag.Int("mqport", 1883, "Port MQTT Message broker is listening on")
	flag.Parse()
	psqlconn := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", db_host, db_port, db_user, db_pass, db_name)
	client, err := sql.Open("postgres", psqlconn)
	CheckError(err)

	defer client.Close()

	err = client.Ping()
	if err != nil {
		panic(err)
	}

	mqtt_start(mq_host, *mqtt_port, mq_user, mq_pass, client)

	handler := func(handler func(w http.ResponseWriter, r *http.Request, client *sql.DB)) func(w http.ResponseWriter, r *http.Request) {
		return func(w http.ResponseWriter, r *http.Request) {
			handler(w, r, client)
		}
	}

	println("Hello World")
	r := mux.NewRouter()
	r.HandleFunc("/api/data.csv", handler(dump_data)).Methods("GET")
	r.HandleFunc("/api/metrics", handler(prometheus_scrape)).Methods("GET")
	http.Handle("/", r)
	err = http.ListenAndServe(*listen_port, nil)
	if err != nil {
		println(err)
	}
}
