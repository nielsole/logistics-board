-- Its own table so we have a list of boards even when nothing is on them.
CREATE TABLE logistics_boards (
    id      SERIAL PRIMARY KEY,
    board_name   text UNIQUE
);

CREATE TABLE logistics_products (
    id      SERIAL PRIMARY KEY,
    ean   TEXT UNIQUE,
    weight  REAL,
    label   text
);

CREATE TABLE logistics_slots (
    id      SERIAL PRIMARY KEY,
    board_name   TEXT references logistics_boards(board_name),
    x_start   REAL,
    x_end    REAL,
    y_start   REAL DEFAULT 0.0,
    y_end     REAL DEFAULT 1.0,
    product_id INT references logistics_products(id),
    amount  integer
);
