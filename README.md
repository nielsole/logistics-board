# Learnings

* Make sure to select an HX711 module with selectable RATE (e.g.: https://de.aliexpress.com/item/33041823995.html?spm=a2g0o.productlist.0.0.719092b2VN4ECb&algo_pvid=d2ff350f-6829-451d-af78-a8d8201cf52c&algo_expid=d2ff350f-6829-451d-af78-a8d8201cf52c-14&btsid=2100bdd016158385292265547eaa04&ws_ab_test=searchweb0_0,searchweb201602_,searchweb201603_) maybe even with an external oscillator
* Connect all HX711 on separate pins (do not use HX711-multi, except maybe with an external oscillator)

# Discussion
## Temperature Measurement
* Temperature adjustement not needed in ventilated rooms
* Use LM35 over TMP36 next time (much more accurate) (Maybe with it's own HX711 ^^ ?)
* DHT11 might be interesting for humidity
* Use Case: Monitor Storage conditions instead of temperature adjustment

## Browser UI / Integration into rest of inventory
* Simplest to stay with Google Sheets approach
* Use IMPORTDATA with CSV for hourly updates (which would be enough, as it takes me > 20 minutes from leaving flat -> reading list, so only the last 40 minutes before leaving *MIGHT* be stale data)

## Configuration / State Management / LED UI / Object Detection
### Object Detection
* Weight distribution should be linear, so position of center of mass is easily calculatable
* Calibration (scale factor) might also have to be hardcoded (possibly this is calibrated in the factory)
### State Management
Flow could be:
* Device generates events (MQTT)
  * Item is added / removed
  * LED strip shows detected location
  * LED strip blinks with color
  * Publish changed weights and location via MQTT (maybe just X-axis and total weight initially)
* Server publishes "new state valid"(product change detected) / "new state invalid"(product change could not be accurately identified)
* Device turns green (<1S) or red (10 seconds or until item is removed / added again)

### Configuration of objects

* Keep on server
* Initially only x-divided?
* Later: Y-divided, weight-multiplexed
* Configuration management via Google Sheet? https://stackoverflow.com/questions/14742350/google-apps-script-make-http-post 
